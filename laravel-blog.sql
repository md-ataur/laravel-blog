-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2020 at 06:54 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel-blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Nasir', 'nasir@gmail.com', NULL, '$2y$10$yQ6k4vJPT9hL059QQfGRV.lLVRMNHU00FJ35IlXpPL7vZHZFdprke', 1, '2020-06-26 07:58:05', '2020-07-01 07:33:44'),
(8, 'Hamid', 'hamid@gamil.com', NULL, '$2y$10$aQpFXmlWPSFWGgYuIrJrM.0h1P.X9UcD.ZIRQW.lJMsvN3PCmKRlO', 1, '2020-06-28 09:47:43', '2020-07-06 09:59:49'),
(9, 'Admin', 'admin@gmail.com', NULL, '$2y$10$uwYNVcqZ9z13yLFbaJmgve4XlyBFxHgAbZtnoxLuwuqtoYrUrATJ.', 1, '2020-06-29 06:28:01', '2020-06-29 06:28:01');

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `admin_id`, `role_id`, `created_at`, `updated_at`) VALUES
(12, 8, 6, '2020-06-28 09:47:56', '2020-06-28 09:47:56'),
(14, 3, 4, '2020-06-29 06:12:16', '2020-06-29 06:12:16'),
(16, 9, 9, '2020-07-07 09:41:18', '2020-07-07 09:41:18');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Laravel', 'laravel', '2020-06-09 08:30:11', '2020-06-09 08:30:11'),
(2, 'Programming', 'programming', '2020-06-09 08:30:21', '2020-06-09 08:30:21'),
(3, 'PHP', 'php', '2020-06-09 08:30:28', '2020-06-09 08:30:28');

-- --------------------------------------------------------

--
-- Table structure for table `category_posts`
--

CREATE TABLE `category_posts` (
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_posts`
--

INSERT INTO `category_posts` (`post_id`, `category_id`, `created_at`, `updated_at`) VALUES
(4, 1, '2020-06-11 03:56:15', '2020-06-11 03:56:15'),
(4, 2, '2020-06-11 03:56:15', '2020-06-11 03:56:15'),
(5, 1, '2020-06-11 04:01:29', '2020-06-11 04:01:29'),
(5, 2, '2020-06-11 04:01:29', '2020-06-11 04:01:29'),
(6, 2, '2020-06-11 04:02:37', '2020-06-11 04:02:37'),
(7, 1, '2020-06-11 04:03:13', '2020-06-11 04:03:13'),
(8, 1, '2020-06-11 04:04:21', '2020-06-11 04:04:21'),
(8, 2, '2020-06-11 04:04:21', '2020-06-11 04:04:21'),
(9, 1, '2020-06-11 04:04:52', '2020-06-11 04:04:52'),
(9, 2, '2020-06-11 04:04:52', '2020-06-11 04:04:52'),
(11, 2, '2020-06-11 04:05:38', '2020-06-11 04:05:38'),
(10, 2, '2020-06-11 10:53:50', '2020-06-11 10:53:50'),
(10, 3, '2020-06-11 10:53:50', '2020-06-11 10:53:50'),
(14, 1, '2020-06-12 08:36:46', '2020-06-12 08:36:46'),
(14, 2, '2020-06-12 08:36:46', '2020-06-12 08:36:46');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `post_id`, `created_at`, `updated_at`) VALUES
(10, 1, 9, '2020-08-30 07:26:04', '2020-08-30 07:26:04'),
(17, 1, 11, '2020-08-30 09:44:07', '2020-08-30 09:44:07'),
(26, 3, 14, '2020-08-30 12:55:13', '2020-08-30 12:55:13'),
(28, 3, 11, '2020-08-30 12:55:16', '2020-08-30 12:55:16'),
(31, 3, 7, '2020-08-30 12:55:30', '2020-08-30 12:55:30'),
(33, 3, 9, '2020-08-30 13:43:09', '2020-08-30 13:43:09');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2019_08_19_000000_create_failed_jobs_table', 1),
(33, '2020_05_23_231146_create_posts_table', 1),
(34, '2020_05_23_231854_create_tags_table', 1),
(35, '2020_05_23_232023_create_categories_table', 1),
(36, '2020_05_23_232157_create_category_posts_table', 1),
(37, '2020_05_23_232748_create_admins_table', 1),
(38, '2020_05_23_233129_create_roles_table', 1),
(39, '2020_05_23_233325_create_admin_roles_table', 1),
(40, '2020_06_09_125816_create_post_tags_table', 1),
(41, '2014_10_12_100000_create_password_resets_table', 2),
(43, '2020_08_29_154626_create_likes_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ataur@gmail.com', '$2y$10$OGkqsx/Ovhtr7sjfwEu1j.KiZxX.eDAVMnq9VdAbUIYguIsyRyXoG', '2020-06-12 17:21:23');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission_for` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `permission_for`, `created_at`, `updated_at`) VALUES
(1, 'Post-Publish', 'post', '2020-06-19 09:02:17', '2020-06-19 09:19:27'),
(4, 'Post-Create', 'post', '2020-06-19 09:20:08', '2020-06-19 09:20:08'),
(5, 'Post-Update', 'post', '2020-06-19 09:20:25', '2020-06-19 09:20:25'),
(6, 'Post-Delete', 'post', '2020-06-19 09:20:38', '2020-06-19 09:20:38'),
(7, 'User-Create', 'user', '2020-06-19 09:20:51', '2020-06-19 09:20:51'),
(8, 'User-Update', 'user', '2020-06-19 09:21:03', '2020-06-19 09:21:03'),
(9, 'User-Delete', 'user', '2020-06-19 09:21:15', '2020-06-19 09:21:15'),
(10, 'Tag-CRUD', 'other', '2020-06-19 09:22:05', '2020-06-19 09:22:05'),
(11, 'Category-CRUD', 'other', '2020-06-19 09:22:15', '2020-06-19 09:22:15');

-- --------------------------------------------------------

--
-- Table structure for table `permission_roles`
--

CREATE TABLE `permission_roles` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_roles`
--

INSERT INTO `permission_roles` (`role_id`, `permission_id`) VALUES
(7, 1),
(8, 1),
(8, 6),
(4, 5),
(4, 6),
(6, 5),
(9, 7),
(9, 8),
(9, 9),
(6, 10),
(6, 11),
(8, 10),
(8, 11),
(9, 1),
(9, 4),
(9, 5),
(9, 6),
(9, 10),
(9, 11),
(4, 8);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `subtitle`, `slug`, `body`, `status`, `posted_by`, `image`, `created_at`, `updated_at`) VALUES
(4, 'This is 2nd post', 'Second Post', 'second-post', '<p><span style=\"font-size: 1rem;\">Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><br></p><div><div><span style=\"font-size: 1rem;\">Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><br></div></div><div><br></div>', 1, NULL, 'public/bfQz6iSmqZbqHgPBAfzni6kVbMCugy4bd0hDmigw.png', '2020-06-11 03:56:15', '2020-06-11 12:03:59'),
(5, 'The third Post', 'Third post', 'third-post', '<p><span style=\"font-size: 1rem;\">Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><br></p><div><span style=\"font-size: 1rem;\">Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><br></div><div><br></div>', 1, NULL, NULL, '2020-06-11 04:01:29', '2020-06-11 04:01:29'),
(6, 'The fourth Post', 'Fourth Post', 'fourth-post', '<p><span style=\"font-size: 1rem;\">Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><br></p><div><span style=\"font-size: 1rem;\">Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><br></div><div><br></div>', 1, NULL, NULL, '2020-06-11 04:02:36', '2020-06-11 04:02:36'),
(7, 'The six post', 'Six Post', 'six-post', '<p><span style=\"font-size: 1rem;\">Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><br></p><div><span style=\"font-size: 1rem;\">Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><br></div><div><br></div>', 1, NULL, NULL, '2020-06-11 04:03:13', '2020-06-11 04:03:13'),
(8, 'The Seven Post', 'Seven Post', 'seven-post', '<p>Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br></p>', 1, NULL, 'public/IXA8Q49jwKNspRQZbPoAfBR1Zq9XKJ3lzpdOHrTi.jpeg', '2020-06-11 04:04:21', '2020-06-12 09:26:32'),
(9, 'The Eight Post', 'Eight Post', 'eight-post', '<p>Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br></p>', 1, NULL, 'public/xp3ov4sSxAQYuHeNk3BlREMZDGqag3UkJjInkSS2.jpeg', '2020-06-11 04:04:52', '2020-06-12 09:25:21'),
(10, 'The nine post', 'Nine Post', 'nine-post', '<p>Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', NULL, NULL, NULL, '2020-06-11 04:05:10', '2020-06-11 04:05:10'),
(11, 'The ten post', 'Ten post', 'ten-post', '<p>Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in&nbsp; in voluptate velit. Ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 1, NULL, 'public/Wum8ys0fwU3KkCbvXAvWb63cEEgC1u0TSkWOJYWc.jpeg', '2020-06-11 04:05:38', '2020-06-12 09:24:56'),
(14, 'A budget not of its time', 'AHM Mustafa Kamal', 'budget', '<p style=\"margin-right: 0px; margin-bottom: 21px; margin-left: 0px; font-family: &quot;Droid Serif&quot;, serif; line-height: 24px;\"><span style=\"font-size: 1rem;\">He or she wants to give the sense that underneath the aspirational abstractions of populist politics is a solid grasp of numbers; that right behind the sunniness of prime-ministerial rhetoric there is a capable and hard-headed person, armed with a pocket calculator.</span><br></p><p style=\"margin-right: 0px; margin-bottom: 21px; margin-left: 0px; font-family: &quot;Droid Serif&quot;, serif; line-height: 24px;\">And AHM Mustafa Kamal seems to have failed on all three counts as he took the podium yesterday to deliver the budget for fiscal 2020-21 -- against the backdrop of potentially the biggest public health emergency and economic downturn in generations.</p><p style=\"margin-right: 0px; margin-bottom: 21px; margin-left: 0px; font-family: &quot;Droid Serif&quot;, serif; line-height: 24px;\">The coronavirus rampage has magnified destitution and stands to unravel the brilliant progress made in poverty alleviation over the past decade. So, like health services before, there was an acute need to ramp up social protection to help poor households weather the pandemic.<br></p>', 1, NULL, 'public/nfl0M8NbgmpBzIWTN7sCLOpgTlssDkkMj0uojplW.png', '2020-06-12 08:36:46', '2020-06-12 08:36:46');

-- --------------------------------------------------------

--
-- Table structure for table `post_tags`
--

CREATE TABLE `post_tags` (
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tags`
--

INSERT INTO `post_tags` (`post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(4, 1, '2020-06-11 03:56:15', '2020-06-11 03:56:15'),
(5, 1, '2020-06-11 04:01:29', '2020-06-11 04:01:29'),
(6, 1, '2020-06-11 04:02:37', '2020-06-11 04:02:37'),
(6, 2, '2020-06-11 04:02:37', '2020-06-11 04:02:37'),
(7, 1, '2020-06-11 04:03:13', '2020-06-11 04:03:13'),
(8, 2, '2020-06-11 04:04:21', '2020-06-11 04:04:21'),
(9, 1, '2020-06-11 04:04:52', '2020-06-11 04:04:52'),
(10, 2, '2020-06-11 04:05:11', '2020-06-11 04:05:11'),
(11, 2, '2020-06-11 04:05:38', '2020-06-11 04:05:38'),
(14, 2, '2020-06-12 08:36:47', '2020-06-12 08:36:47');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(4, 'Contributor', '2020-06-18 12:14:11', '2020-08-31 06:23:37'),
(6, 'Editor', '2020-06-20 06:23:43', '2020-06-20 06:23:43'),
(8, 'Publisher', '2020-06-20 09:00:31', '2020-06-20 09:00:31'),
(9, 'Admin', '2020-07-07 09:40:33', '2020-07-07 09:40:33');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Tutorial', 'tutorial', '2020-06-09 08:29:49', '2020-06-09 08:29:49'),
(2, 'Education', 'education', '2020-06-09 08:29:59', '2020-06-09 08:29:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ataur Rahman', 'ataur@gmail.com', NULL, '$2y$10$.k8SkSYTXsosMGkX0x3sOemB9hJB0CAtqjCeDgb.IGq6gf9zoc/Qe', 'xVZAjQkK5EQLy1G0KtTUFKuGKC4iphB30cEXKQwLhsIbsNk2nkZ8MQVqLL4j', '2020-06-12 10:17:35', '2020-06-12 10:17:35'),
(3, 'Kawsar', 'kawsar@gmail.com', NULL, '$2y$10$VZGniFaJmp/Y8h1YIe2MA.ti/kGCDwzcJ3KCE0lb6dIqyL5FoRr7.', NULL, '2020-08-30 12:33:03', '2020-08-30 12:33:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_posts`
--
ALTER TABLE `category_posts`
  ADD KEY `category_posts_post_id_foreign` (`post_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD KEY `post_tags_post_id_foreign` (`post_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_posts`
--
ALTER TABLE `category_posts`
  ADD CONSTRAINT `category_posts_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD CONSTRAINT `post_tags_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
