<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// User Routes
Route::group(['namespace' => 'User'], function(){
    Route::get('/', 'HomeController@index' )->name('index');
    
    /* I want to get the post via post slug */
    Route::get('post/{slug}', 'PostController@post' )->name('post');

    /* Get category posts via category slug */
    Route::get('post/category/{category}', 'HomeController@category')->name('category');

    /* Get tag posts via tag slug */
    Route::get('post/tag/{tag}', 'HomeController@tag')->name('tag');
    
    /* Vue Route */
    Route::post('getPosts', 'PostController@getAllPost');
    Route::post('saveLike', 'PostController@saveLike');

    
});


// Admin Routes
Route::group(['namespace' => 'Admin', 'middleware' => 'auth:admin'], function(){
    // Admin Home Route
    Route::get('admin/home', 'DashbController@index')->name('admin.home');    
    
    // Admin User Route
    Route::resource('admin/user', 'UserController');
    
    // Role Route
    Route::resource('admin/role', 'RoleController');
   
    // Permission Route
    Route::resource('admin/permission', 'PermissionController');
    
    // Admin Post Route
    Route::resource('admin/post', 'PostController');

    // Admin Category Route
    Route::resource('admin/category', 'CategoryController');

    // Admin Tag Route
    Route::resource('admin/tag', 'TagController');  

});

// Admin Auth Route
Route::get('admin-login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('admin-login', 'Admin\Auth\LoginController@login');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

