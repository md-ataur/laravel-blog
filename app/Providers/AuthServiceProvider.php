<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        /**
         * Think of gates and policies like routes and controllers. 
         * Custom code
         */
        Gate::define('create-post', 'App\Policies\PostPolicy@create');
        Gate::define('update-post', 'App\Policies\PostPolicy@update');
        Gate::define('delete-post', 'App\Policies\PostPolicy@delete');
        Gate::define('category-post', 'App\Policies\PostPolicy@category');
        Gate::define('post-tag', 'App\Policies\PostPolicy@tag');

        Gate::define('user-create', 'App\Policies\AdminPolicy@create');
        Gate::define('user-update', 'App\Policies\AdminPolicy@update');
        Gate::define('user-delete', 'App\Policies\AdminPolicy@delete');
    }
}
