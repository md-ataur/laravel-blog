<?php

namespace App\Http\Controllers\Admin;

use App\Model\admin\role;
use App\Http\Controllers\Controller;
use App\Model\admin\permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller
{

    /* public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('can:user-create');
    } */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('user-create')) {
            $roles = role::all();
            return view('admin.role.role', compact('roles'));
        }else{
            return redirect(route('admin.home'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('user-create')) {
            $permissions = permission::all();
            return view('admin.role.create', compact('permissions'));
        }else{
            return redirect(route('admin.home'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        
        $request->validate([
            'name' => 'required|max:60|unique:roles',
            'permissions' => 'required',
        ]);
        
        $role = new role;
        $role->name = $request->name;     
        /* First you need to role save for role id */   
        $role->save();
        
        $role->permissions()->sync( $request->permissions );
        return redirect(route('role.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('user-create')) {
            $role = role::with('permissions')->find($id);   
            //return $role->permissions;  
            $permissions = permission::all();   
            return view('admin.role.edit', compact('role','permissions'));
        }else{
            return redirect(route('admin.home'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:60',
            'permissions' => 'required',
        ]);
        $role = role::find($id);
        $role->name = $request->name;    
        $role->permissions()->sync( $request->permissions );    
        $role->save();
        return redirect(route('role.index'))->with('message', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = role::find($id);
        $item->delete();
        session()->flash('message','Deleted Successfully');
        return redirect()->back();
    }
}
