<?php

namespace App\Http\Controllers\Admin;

use App\Model\admin\admin;
use App\Model\admin\role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    
    /* public function __construct()
    {
        $this->middleware('auth:admin');        
    } */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('user-create')) {
            $users = admin::with('roles')->get();
            //return $users;
            return view('admin.user.user', compact('users'));
        }else{
            return redirect(route('admin.home'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('user-create')) {
            $roles = role::all();
            return view('admin.user.create', compact('roles'));
        }else{
            return redirect(route('admin.home'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $user = new admin;
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:admins'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $request['password'] = bcrypt($request->password);
        $user = admin::create($request->all());
        $user->roles()->sync( $request->roles );
        return redirect(route('user.index'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('user-update')) {
            $user = admin::with( 'roles' )->find($id);         
            $roles = role::all();       
            return view('admin.user.edit', compact('user','roles'));
        }else{
            return redirect(route('admin.home'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],           
        ]);
        $request->status ? : $request['status'] = 0;
        
        admin::find($id)->update($request->except('_token', '_method'));        
        admin::find($id)->roles()->sync( $request->roles );       
        return redirect(route('user.index'))->with('message', 'User updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        admin::find($id)->delete();        
        return redirect()->back()->with('message', 'User is deleted Successfully');
    }
}
