<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Model\admin\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\Admin\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{  
   
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * As a guest, you have to define the gurd name is admin
     *     
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('admin.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request     
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
       
        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {          
        /* The first() method will return only one record */
        $admin = admin::where('email', $request->email)->first();
        if ($admin === null){
            return $request->only($this->username(), 'password');
        }elseif ($admin->status == 0) {
            return ['email'=>'inactive','password'=>'You are not authorized to access login or Incorrect Password'];
        }else{
            return ['email'=>$request->email,'password'=>$request->password,'status'=>1];
        }
        
        //return $request->only($this->username(), 'password');
    }

    /**
     * You have to pass the parameter of the guard name 
     * 
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
