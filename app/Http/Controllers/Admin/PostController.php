<?php

namespace App\Http\Controllers\Admin;

use App\Model\user\tag;
use App\Model\user\post;
use App\Model\user\category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller 
{

   
    /* public function __construct()
    {
        $this->middleware('auth:admin');
    } */
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $posts = post::all();
        return view( 'admin.post.post', compact( 'posts' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        if (Gate::allows('create-post')) {
            $categories = category::all();
            $tags       = tag::all();
            return view( 'admin.post.create', compact( 'categories', 'tags' ) );
        }else{
            return redirect(route('post.index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {

        //return $request->all();

        $request->validate( [
            'title'    => 'required|max:100',
            'subtitle' => 'required',
            'slug'     => 'required',
            'image'    => 'required|image',
            'body'     => 'required',
        ] );

        if ( $request->hasFile( 'image' ) ) {           
            $imageName   = $request->image->store( 'public' );                       
        }
        $post           = new post;
        $post->title    = $request->title;
        $post->subtitle = $request->subtitle;
        $post->slug     = $request->slug;
        $post->image    = $imageName; 
        $post->status   = $request->status;
        $post->body     = $request->body;        

        /* First you need to post save for post id */
        $post->save();

        $post->categories()->sync( $request->categories );
        $post->tags()->sync( $request->tags );

        return redirect( route( 'post.index' ) );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id ) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        if (Gate::allows('update-post')) {
            $post = post::with( 'tags', 'categories' )->find( $id );        
            $categories = category::all();
            $tags       = tag::all();
            return view( 'admin.post.edit', compact( 'categories', 'tags', 'post' ) );
        }else{
            return redirect(route('post.index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id ) {
        //return $request->all();

        $request->validate( [
            'title'    => 'required|max:100',
            'subtitle' => 'required',
            'slug'     => 'required',
            'image'    => 'image',
            'body'     => 'required',
        ] );

        $post           = post::find( $id );
        $post->title    = $request->title;
        $post->subtitle = $request->subtitle;
        $post->slug     = $request->slug;
        $post->status   = $request->status;
        $post->body     = $request->body;

        if ( $request->hasFile( 'image' ) ) {
            $oldFileName = $post->image;           
            $imageName   = $request->image->store( 'public' );
            $post->image = $imageName;
            // Delete Old File Name
            Storage::delete( $oldFileName );
        }

        $post->categories()->sync( $request->categories );
        $post->tags()->sync( $request->tags );
        $post->save();
        return redirect( route( 'post.index' ) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        $item = post::find( $id );
        Storage::delete($item->image);
        $item->delete();
        session()->flash( 'message', 'Deleted Successfully' );
        return redirect()->back();
    }
}
