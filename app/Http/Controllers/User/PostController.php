<?php

namespace App\Http\Controllers\User;

use App\Model\user\like;
use App\Model\user\post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /* You have to tell Laravel, hei Laravel, I want to get the post vai slug */
    public function post(post $slug)
    {        
        //return $slug;
        return view('user.post', compact('slug'));
    }   

    /**
     * These functions are for Vue
     */
    public function getAllPost(){
        return post::with('likes')->where('status', 1)->orderBy('created_at','DESC')->paginate(4);
    }
    
    public function saveLike(request $request){
        
        $likecheck = like::where(['user_id'=>Auth::id(), 'post_id'=>$request->id])->first();
        
        /* If the user has like value then we are going to delete */
        if($likecheck){
            like::where(['user_id'=>Auth::id(), 'post_id'=>$request->id])->delete();
            return "deleted";
        }else{
            $like = new like;
            $like->user_id = Auth::id();
            $like->post_id = $request->id;
            $like->save();        
        }
    }
}
