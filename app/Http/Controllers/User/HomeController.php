<?php

namespace App\Http\Controllers\User;

use App\Model\user\post;
use App\Model\user\category;
use App\Model\user\tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        /* This is just for post link/pagination */
        $posts = post::where('status', 1)->orderBy('created_at','DESC')->paginate(4);
        return view('user.blog', compact('posts'));
    }

    /* Get category posts via category slug */
    public function category(category $category)
    {
        /**
         * Model > user > category.php
         * posts();  Eloquent Relationship function
         */
        $posts = $category->posts();
        return view('user.blog', compact('posts'));

    }

    /* Get tag posts via tag slug */
    public function tag(tag $tag)
    {
        /**
         * Model > user > tag.php
         * posts();  Eloquent Relationship function
         */
        $posts = $tag->posts();
        return view('user.blog', compact('posts'));

    }
}
