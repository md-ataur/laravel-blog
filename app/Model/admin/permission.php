<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class permission extends Model
{
    /**
     * Many To Many Relationship 
     * One permission can have many roles
     */
    public function roles(){
        return $this->belongsToMany('App\Model\admin\role', 'permission_roles');
    }
}
