<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    /**
     * Many To Many Relationship 
     * One role can assign many permissions
     */ 
    public function permissions(){
        return $this->belongsToMany('App\Model\admin\permission', 'permission_roles');
    }
    
   
    /**
     * Many To Many Relationship 
     * One role can have many users
     */   
    public function admins(){
        return $this->belongsToMany('App\Model\admin\admin', 'admin_roles');
    }
}
