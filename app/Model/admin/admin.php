<?php

namespace App\Model\admin;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class admin extends Authenticatable
{
    use Notifiable;  

    /* Accessors and mutators allow you to format Eloquent attribute values */
    public function getNameAttribute($value){
        return ucfirst($value);
    }

    /**
     * Many To Many Relationship 
     * One user can have many roles
     */
    public function roles(){
        return $this->belongsToMany('App\Model\admin\role', 'admin_roles')->withTimestamps();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
