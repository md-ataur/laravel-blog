<?php

namespace App\Model\user;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    /**
     * Many To Many Relationship 
     * One post can have many tags
     */ 
    public function tags()
    {
        return $this->belongsToMany('App\Model\user\tag', 'post_tags')->withTimestamps();
    }
    
    /**
     * Many To Many Relationship 
     * One post can have many categories
     */ 
    public function categories()
    {
        return $this->belongsToMany('App\Model\user\category', 'category_posts')->withTimestamps();
    }

    /* It will get the post via slug */
    public function getRouteKeyName(){
        return 'slug';
    }
    
    /* Accessors and mutators allow you to format Eloquent attribute values */
    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->diffForHumans();
    }
    
    /**
     * Many To Many Relationship 
     * One post can have many likes
     */ 
    public function likes(){
        return $this->hasMany('App\Model\user\like');
    }

    /* Accessors and mutators allow you to format Eloquent attribute values */
    public function getSlugAttribute($value){        
        return route('post', $value);
    }
   
}
