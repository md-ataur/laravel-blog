<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class tag extends Model
{
    /**
     * Many To Many Relationship 
     * One tag can have many posts
     */ 
    public function posts()
    {
        return $this->belongsToMany('App\Model\user\post', 'post_tags')->orderBy('created_at','DESC')->paginate(5);
    }

    /* It will get the category posts via category slug*/
    public function getRouteKeyName(){
        return 'slug';
    }
}
