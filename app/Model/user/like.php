<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class like extends Model
{
    public function post(){
        return $this->belongsTo('App\Model\user\post', 'likes');        
    }
}
