<?php

namespace App\Policies;

use App\Model\admin\admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the admin can view any models.
     *
     * @param  \App\Model\admin\admin  $admin
     * @return mixed
     */
    public function viewAny(admin $admin)
    {
        //
    }

    /**
     * Determine whether the admin can view the model.
     *
     * @param  \App\Model\admin\admin  $admin
     * @param  \App\admin  $admin
     * @return mixed
     */
    public function view(admin $admin)
    {
        //
    }

    /**
     * Determine whether the admin can create models.
     *
     * @param  \App\Model\admin\admin  $admin
     * @return mixed
     */
    public function create(admin $admin)
    {
        return $this->getPermission($admin, 7);
    }

    /**
     * Determine whether the admin can update the model.
     *
     * @param  \App\Model\admin\admin  $admin
     * @param  \App\admin  $admin
     * @return mixed
     */
    public function update(admin $admin)
    {
        return $this->getPermission($admin, 8);
    }

    /**
     * Determine whether the admin can delete the model.
     *
     * @param  \App\Model\admin\admin  $admin
     * @param  \App\admin  $admin
     * @return mixed
     */
    public function delete(admin $admin)
    {
        return $this->getPermission($admin, 9);
    }

    /**
     * Determine whether the admin can restore the model.
     *
     * @param  \App\Model\admin\admin  $admin
     * @param  \App\admin  $admin
     * @return mixed
     */
    public function restore(admin $admin)
    {
        //
    }

    /**
     * Determine whether the admin can permanently delete the model.
     *
     * @param  \App\Model\admin\admin  $admin
     * @param  \App\admin  $admin
     * @return mixed
     */
    public function forceDelete(admin $admin)
    {
        //
    }

    /* This custom function for code simplicity */
    protected function getPermission($admin, $permission_id)
    {
        foreach ($admin->roles as $role) {
            foreach ($role->permissions as $permission) {
                if( $permission->id == $permission_id){
                    return true;
                }
            }
        }
        return false;
    }
}
