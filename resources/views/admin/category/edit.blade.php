@extends('admin.layouts.app')

@section('main-content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add New Category</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Category</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <form action="{{route('category.update', $category->id)}}" method="post">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="card-body">                            
                            <div class="col-md-4 offset-4">
                                <div class="form-group">
                                    <label for="name">Category Title</label>
                                    <input type="text" class="form-control" name="name" value="{{$category->name}}">
                                    @if ($errors->has('name'))                                            
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="slug">Slug</label>
                                    <input type="text" class="form-control" name="slug" value="{{$category->slug}}">
                                    @if ($errors->has('slug'))                                            
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('slug') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="update" class="btn btn-primary">Update</button>                                    
                                    <a href="{{route('category.index')}}" class="btn btn-secondary">Back</a>
                                </div>
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>           
        </div>       
    </section>
</div>
@endsection