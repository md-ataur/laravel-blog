@extends('admin.layouts.app')

@section('main-content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add New Role</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Role</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <form action="{{route('role.store')}}" method="post">

                        {{ csrf_field() }}

                        <div class="card-body">
                            <div class="col-md-6 offset-3">
                                <div class="form-group">
                                    <label for="name">Role Name</label>
                                    <input type="text" class="form-control" name="name">
                                    @if ($errors->has('name'))
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for="permission">Post Permissions</label>
                                            <div class="form-group">
                                                @foreach ($permissions as $permission)
                                                    @if ('post' == $permission->permission_for)
                                                        <div class="form-check-label"><input type="checkbox" name="permissions[]" value="{{ $permission->id }}">
                                                            {{ $permission->name }}
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="permission">User Permissions</label>
                                            <div class="form-group">
                                                @foreach ( $permissions as $permission )
                                                    @if ('user' == $permission->permission_for)
                                                        <div class="form-check-label"><input type="checkbox" name="permissions[]" value="{{ $permission->id }}">
                                                            {{ $permission->name }}
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="permission">Other Permissions</label>
                                            <div class="form-group">
                                                @foreach ( $permissions as $permission )
                                                    @if ('other' == $permission->permission_for)
                                                        <div class="form-check-label"><input type="checkbox" name="permissions[]" value="{{ $permission->id }}">
                                                            {{ $permission->name }}
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->has('permissions'))
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('permissions') }}</p>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{route('role.index')}}" class="btn btn-secondary">Back</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection