@extends('admin.layouts.app')

@section('main-content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add New User</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                        <li class="breadcrumb-item active">User</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <form action="{{route('user.store')}}" method="post">

                        {{ csrf_field() }}

                        <div class="card-body">                            
                            <div class="col-md-6 offset-3">
                                <div class="form-group">
                                    <label for="name">User Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))                                            
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))                                            
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('email') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))                                            
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('password') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password">Confirm Password</label>
                                    <input type="password" class="form-control" name="password_confirmation">
                                    @if ($errors->has('password_confirmation'))                                            
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('password_confirmation') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-check-label" for="status"><input type="checkbox" name="status" @if (old('status') == 1)
                                        checked
                                    @endif value="1" > Status</label>                                   
                                </div>
                                <div class="form-group">
                                    <label for="role">Assign Role</label>
                                    <div class="row">
                                        @foreach ($roles as $role)
                                            <div class="col-lg-3">
                                                <input type="checkbox" name="roles[]" value="{{ $role->id }}">
                                                <label class="form-check-label">{{ $role->name }}</label>                                               
                                            </div> 
                                        @endforeach
                                    </div>                                    
                                    @if ($errors->has('role'))                                            
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('role') }}</p>
                                    @endif
                                </div>                                
                                <div class="form-group">
                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{route('user.index')}}" class="btn btn-secondary">Back</a>
                                </div>
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>           
        </div>       
    </section>
</div>
@endsection