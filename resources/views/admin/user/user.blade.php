@extends('admin.layouts.app')

@section('headSection')
<link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">	
@endsection

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>All Users</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
						<li class="breadcrumb-item active">user</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="card">
			<div class="card-header">
				@include('admin.partial.message')
				<center><a href="{{route('user.create')}}" class="btn btn-success">Add New User</a></center>
			</div>
			<div class="card-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>SL No</th>
							<th>User Name</th>
							<th>Email</th>
							<th>Role</th>
							<th>Status</th>
							@can('user-update', App\Model\admin\admin::class)
								<th>Edit</th>
							@endcan
							@can('user-delete', App\Model\admin\admin::class)
								<th>Delete</th>
							@endcan
						</tr>
					</thead>
					<tbody>
						@foreach ($users as $user)
							<tr>
								<td>{{$loop->index + 1}}</td>								
								<td>{{$user->name}}</td>								
								<td>{{$user->email}}</td>
								<td>@foreach ($user->roles as $user_role)
									{{$user_role->name}},
								@endforeach</td>
								<td>{{ $user->status == 1 ? 'Active' : 'Not Active' }}</td>
								@can('user-update', App\Model\admin\admin::class)
									<td><span><a class="btn btn-info" href="{{route('user.edit', $user->id)}}"><i class="far fa-edit"></i></a></span></td>
								@endcan
								@can('user-delete', App\Model\admin\admin::class)
									<td>								
										<form class="ml-2" action="{{route('user.destroy', $user->id)}}" method="post">
											{{csrf_field()}}
											{{method_field("DELETE")}}
											<span><button onclick="if(confirm('Are you sure, You want to delete this!')){}else{event.preventDefault();}" type="submit" name="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></span>				    			
										</form>
									</td>
								@endcan
							</tr>
						@endforeach						
					</tbody>
					<tfoot>
						<tr>
							<th>SL No</th>
							<th>User Name</th>
							<th>Email</th>
							<th>Role</th>
							<th>Status</th>
							@can('user-update', App\Model\admin\admin::class)
								<th>Edit</th>
							@endcan
							@can('user-delete', App\Model\admin\admin::class)
								<th>Delete</th>
							@endcan
						</tr>
					</tfoot>
				</table>
			</div>			
		</div>
		<!-- /.card -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('footerSection')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script>
	$("#example1").DataTable({
		"responsive": true,
		"autoWidth": false,
	}); 
</script>
@endsection