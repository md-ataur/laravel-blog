@extends('admin.layouts.app')

@section('main-content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add New Permission</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Permission</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <form action="{{route('permission.store')}}" method="post">

                        {{ csrf_field() }}

                        <div class="card-body">                            
                            <div class="col-md-4 offset-4">
                                <div class="form-group">
                                    <label for="name">Permission Name</label>
                                    <input type="text" class="form-control" name="name">
                                    @if ($errors->has('name'))                                            
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>                               
                                <div class="form-group">
                                    <label for="name">Permission For</label>
                                    <select class="form-control" name="permission_for">
                                        <option selected disabled>Select Permision</option>
                                        <option value="user">User</option>
                                        <option value="post">Post</option>
                                        <option value="other">Other</option>
                                    </select>
                                    @if ($errors->has('permission_for'))                                            
                                        <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('permission_for') }}</p>
                                    @endif
                                   
                                </div>                               
                                <div class="form-group">
                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{route('permission.index')}}" class="btn btn-secondary">Back</a>
                                </div>
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>           
        </div>       
    </section>
</div>
@endsection