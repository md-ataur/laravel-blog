@extends('admin.layouts.app')

@section('headSection')
<link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
@endsection

@section('main-content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Post</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Post</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    {{-- @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="alert alert-danger">{{ $error }}</p>
                        @endforeach
                    @endif --}}
                    
                    <form action="{{route('post.update',$post->id)}}" method="post" enctype="multipart/form-data">  

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="title">Post Title</label>
                                    <input type="text" class="form-control" name="title" value="{{$post->title}}">
                                        @if ($errors->has('title'))                                            
                                            <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('title') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="subtitle">Subtitle</label>
                                        <input type="text" class="form-control" name="subtitle" value="{{$post->subtitle}}">
                                        @if ($errors->has('subtitle'))                                            
                                            <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('subtitle') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="slug">Slug</label>
                                        <input type="text" class="form-control" name="slug" value="{{$post->slug}}">
                                        @if ($errors->has('slug'))                                            
                                            <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('slug') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputFile">Post Image</label>
                                        <div class="input-group">
                                            <div class="image-file mb-2">
                                                <input type="file" name="image" class="custom-file-input">
                                                <label class="custom-file-label">Choose Image</label>  
                                                @if ($errors->has('image'))                                            
                                                    <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('image') }}</p>
                                                @endif                                                
                                            </div>
                                            <div><img style="width:30%;" src="{{Storage::disk('local')->url($post->image)}}" /></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Select Category</label>
                                        <div class="select2-purple">
                                            <select name="categories[]" class="select2" multiple="multiple"                                                
                                                data-dropdown-css-class="select2-purple" style="width: 100%;">
                                                @foreach ($categories as $category)
                                                    <option value="{{$category->id}}"
                                                        @foreach ($post->categories as $postCategory)
                                                            @if ($postCategory->id == $category->id)
                                                                {{'selected'}}
                                                            @endif
                                                        @endforeach
                                                        >{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Select Tag</label>
                                        <div class="select2-purple">
                                            <select name="tags[]" class="select2" multiple="multiple"
                                                data-dropdown-css-class="select2-purple" style="width: 100%;">
                                                @foreach ($tags as $tag)
                                                    <option value="{{$tag->id}}"
                                                        @foreach ($post->tags as $postTag)
                                                            @if ($postTag->id == $tag->id)
                                                                {{'selected'}}
                                                            @endif
                                                        @endforeach
                                                        >{{$tag->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" name="status" value="1" class="form-check-input" @if ( 1 == $post->status ) {{'checked'}} @endif>
                                        <label class="form-check-label">Publish</label>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4">
                                <textarea class="textarea" name="body" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid#dddddd; padding: 10px;">
                                    {{$post->body}}
                                </textarea>
                                @if ($errors->has('body'))                                            
                                    <p style="color:#ff0000; margin-top:5px;">{{ $errors->first('body') }}</p>
                                @endif
                            </div>
                            <button type="submit" name="update" class="btn btn-primary">Update</button>
                            <a href="{{route('post.index')}}" class="btn btn-secondary">Back</a>
                        </div>                        
                    </form>                    
                </div>
            </div>            
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</div>

@endsection

@section('footerSection')
<script src="{{asset('admin/plugins/select2/js/select2.full.min.js')}}"></script>
<script>
    $('.select2').select2();
</script>
@endsection