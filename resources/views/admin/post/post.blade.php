@extends('admin.layouts.app')

@section('headSection')
<link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">	
@endsection

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>All Posts</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
						<li class="breadcrumb-item active">Post</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="card">
			<div class="card-header">
				@include('admin.partial.message')
				@can('create-post', App\Model\user\post::class)
					<center><a href="{{route('post.create')}}" class="btn btn-success">Add New Post</a></center>					
				@endcan
			</div>
			<div class="card-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>SL No</th>
							<th>Post Title</th>
							<th>Subtitle</th>
							<th>slug</th>
							<th>Created At</th>
							@can('update-post', App\Model\user\post::class)
								<th>Edit</th>
							@endcan
							@can('delete-post', App\Model\user\post::class)
								<th>Delete</th>
							@endcan
						</tr>
					</thead>
					<tbody>
						@foreach ($posts as $post)
							<tr>
								<td>{{$loop->index + 1}}</td>								
								<td>{{$post->title}}</td>
								<td>{{$post->subtitle}}</td>
								<td>{{$post->slug}}</td>
								<td>{{$post->created_at}}</td>
								
								@can('update-post', App\Model\user\post::class)
									<td><span><a class="btn btn-info" href="{{route('post.edit', $post->id)}}"><i class="far fa-edit"></i></a></span></td>
								@endcan

								@can('delete-post', App\Model\user\post::class)
									<td>
										<form class="ml-2" action="{{route('post.destroy', $post->id)}}" method="post">
											{{csrf_field()}}
											{{method_field("DELETE")}}
										<span><button onclick="if(confirm('Are you sure, You want to delete this!')){}else{event.preventDefault();}" type="submit" name="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></span>				    			
										</form>
									</td>
								@endcan
							</tr>
						@endforeach
						
					</tbody>
					<tfoot>
						<tr>
							<th>SL No</th>
							<th>Post Title</th>
							<th>Subtitle</th>
							<th>slug</th>
							<th>Created At</th>
							@can('update-post', App\Model\user\post::class)
								<th>Edit</th>
							@endcan
							@can('delete-post', App\Model\user\post::class)
								<th>Delete</th>
							@endcan
						</tr>
					</tfoot>
				</table>
			</div>			
		</div>
		<!-- /.card -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('footerSection')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script>
	$("#example1").DataTable({
		"responsive": true,
		"autoWidth": false,
	}); 
</script>
@endsection