@extends('user.app')

@section('bg-image', asset('user/img/home-bg.jpg'))
@section('title', ('Bitfumes Blog'))
@section('sub-heading', ('A Blog Theme by Start Bootstrap'))

@section('main-content')
    <!-- Main Content -->
    <div id="app" class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
               {{-- @foreach ($posts as $post)                
                    <div class="post-preview">
                        <a href="{{'/post/'.$post->slug}}">
                            <h2 class="post-title">
                                {{$post->title}}
                            </h2>
                            <h3 class="post-subtitle">
                                {{$post->subtitle}}
                            </h3>
                        </a>
                        <p class="post-meta">Posted by
                            <a href="#">Start Bootstrap</a>
                            {{ $post->created_at->diffForHumans() }}

                            <a href="#">
                                <span>0</span>
                                <i class="fas fa-thumbs-up"></i>
                            </a>
                        </p>
                    </div>
                    <hr>  
                @endforeach --}}         

                <!-- Vue -->
                <posts
                    v-for = 'value in blog'
                    :title = value.title
                    :subtitle = value.subtitle
                    :created_at = value.created_at 
                    :key = value.index 
                    :postid = value.id 
                    :likes = value.likes.length
                    :slug = value.slug
                    login = {{ Auth::check() }}
                    
                ></posts>

                <!-- Pager -->
                <div class="clearfix">
                    {{$posts->links()}}
                    <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
                </div>
            </div>
        </div>
    </div>

    <hr>
@endsection

@section('footer')
<script src="{{ asset('js/app.js') }}"></script>
@endsection