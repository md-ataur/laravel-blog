@extends('user.app')

@section('bg-image', Storage::disk('local')->url($slug->image))
@section('title', $slug->title)
@section('sub-heading', $slug->subtitle)


@section('main-content')
<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <small><b>Created at: </b> {{ $slug->created_at }} </small>
                <div style="float: right;">
                    <small>Category:</small>
                    @foreach ($slug->categories as $category)
                        <small style="margin-right: 3px;">
                            <a href="{{route('category',$category->slug)}}">{{$category->name}}</a>
                        </small>
                    @endforeach
                </div>

                {!! htmlspecialchars_decode($slug->body) !!}

                <div class="tags">
                    <h5>Tags:</h5>
                    @foreach ($slug->tags as $tag)
                        <a href="{{route('tag',$tag->slug)}}">
                            <small>{{$tag->name}}</small>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</article>

<hr>

@endsection